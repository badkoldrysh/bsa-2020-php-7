<?php

namespace App\Http\Controllers;

use App\Entity\Product;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class ProductsController extends Controller
{
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $products = Product::all();
        return view('products', ['products' => $products]);
    }

    public function show(Request $request, $id)
    {
        $product = Product::find($id);
        if (Gate::denies('view', $product)) {
            return redirect($this->redirectTo);
        }

        return view('products.show', ['product' => $product]);
    }

    public function create(Request $request)
    {
        if (Gate::denies('create', Product::class)) {
            return redirect($this->redirectTo);
        }

        return view('products.create');
    }

    public function edit(Request $request, $id)
    {
        $product = Product::find($id);

        if (Gate::denies('update', $product)) {
            return redirect($this->redirectTo);
        }

        return view('products.edit', ['product' => $product]);
    }

    public function delete(Request $request, $id)
    {
        $product = Product::find($id);
        if (Gate::denies('view', [Auth::user(), $product])) {
            return redirect($this->redirectTo);
        }

        $product->delete();
        return redirect($this->redirectTo);
    }

    public function store(Request $request)
    {
        if (Gate::denies('create', )) {
            return redirect('/');
        }

        $product = Product::create(
            [
                'name' => $request->getName(),
                'price' => $request->getPrice(),
                'user_id' => Auth::user()->id,
            ]
        );
        $product->save();

        return redirect($this->redirectTo);
    }
}
