<?php

namespace App\Policies;

use App\Entity\Product;
use App\Entity\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any products.
     *
     * @param  \App\Entity\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the product.
     *
     * @param  \App\Entity\User  $user
     * @param  \App\Product  $product
     * @return mixed
     */
    public function view(User $user, Product $product)
    {
        return true;
    }

    /**
     * Determine whether the user can create products.
     *
     * @param  \App\Entity\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the product.
     *
     * @param  \App\Entity\User  $user
     * @param  \App\Product  $product
     * @return mixed
     */
    public function update(User $user, Product $product)
    {
        if ($this->isAdmin($user)) {
            return true;
        }

        return $this->isUsersProduct($user, $product);
    }

    /**
     * Determine whether the user can delete the product.
     *
     * @param  \App\Entity\User  $user
     * @param  \App\Product  $product
     * @return mixed
     */
    public function delete(User $user, Product $product)
    {
        if ($this->isAdmin($user)) {
            return true;
        }

        return $this->isUsersProduct($user, $product);
    }

    private function isUsersProduct(User $user, Product $product): bool
    {
        return $product->user_id === $user->id;
    }

    private function isAdmin(User $user): bool
    {
        return $user->is_admin;
    }
}
