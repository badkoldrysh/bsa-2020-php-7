<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .product-item a {
                text-decoration: none;
                color: #636b6f;
            }

            .product-item a:hover {
                color: #2f3234;
                font-weight: bold;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/logout') }}">Logout</a>
                        <span>{{ Auth::user()->name }}</span>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Products
                </div>
                <div><a href="/products/create">Add</a></div>
                <div style="text-align: left;">
                    <div class="table-header">
                        <div style="font-weight: bold;">Name - Price ($)</div>
                        @foreach($products as $product)
                            <div class="product-item"><a href="/products/{{ $product->id }}">{{ $product->name }} - {{ $product->price }}</a></div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
